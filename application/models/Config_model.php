<?php
 
class Config_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_config($ds_config)
    {
        return $this->db->get_where('config',array('ds_config'=>$ds_config))->row_array();
    }
            
   
    function update_config($ds_config,$params)
    {
        $this->db->where('ds_config',$ds_config);
        return $this->db->update('config',$params);
    }
}