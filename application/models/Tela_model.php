<?php

class Tela_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
            
    function get_all_telas($params = array())
    {
        $this->db->order_by('descricao', 'asc');
        return $this->db->get('telas')->result_array();
    }
            
    
}
