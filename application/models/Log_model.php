<?php
 
class Log_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_logs($tabela = null, $acao = null, $data = null)
    {
        if(!is_null($tabela)){
        $this->db->where('tabela',$tabela);
        }
        if(!is_null($acao)){
            $this->db->where('acao',$acao);
        }
        if(!is_null($data)){
            $this->db->where('data',$data);
        }
        return $this->db->get('logs')->result_array();
    }
    
    function add_log($params)
    {
        $this->db->insert('logs',$params);
        return $this->db->insert_id();
    }
   
}