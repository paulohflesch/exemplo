<?php
class Usuario_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get usuario by idusuario
     */
    function get_usuario($idusuario)
    {
        return $this->db->get_where('usuarios',array('idusuario'=>$idusuario))->row_array();
    }

    /*
     * Get usuario by email
     */
    function get_usuario_email($email)
    {
        return $this->db->get_where('usuarios',array('email'=>$email))->row_array();
    }
    
    /*
     * Get all usuarios count
     */
    function get_all_usuarios_count()
    {
        $this->db->from('usuarios');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all usuarios
     */
    function get_all_usuarios($params = array())
    {
        $this->db->order_by('idusuario', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('usuarios')->result_array();
    }
        
    /*
     * function to add new usuario
     */
    function add_usuario($params)
    {
        $this->db->insert('usuarios',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update usuario
     */
    function update_usuario($idusuario,$params)
    {
        $this->db->where('idusuario',$idusuario);
        return $this->db->update('usuarios',$params);
    }
    
    /*
     * function to delete usuario
     */
    function delete_usuario($idusuario)
    {
        return $this->db->delete('usuarios',array('idusuario'=>$idusuario));
    }
}
