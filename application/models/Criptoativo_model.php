<?php
 
class Criptoativo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get criptoativo by idcriptoativo
     */
    function get_criptoativo($idcriptoativo)
    {
        return $this->db->get_where('criptoativos',array('idcriptoativo'=>$idcriptoativo))->row_array();
    }
    
    /*
     * Get all criptoativos count
     */
    function get_all_criptoativos_count()
    {
        $this->db->from('criptoativos');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all criptoativos
     */
    function get_all_criptoativos($params = array())
    {
        $this->db->select('criptoativos.idcriptoativo, criptoativos.sigla,criptoativos.ativo, exchanges.sigla exg');
        $this->db->join('exchanges', 'exchanges.idexchange = criptoativos.idexchange','LEFT');
        $this->db->order_by('idcriptoativo', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('criptoativos')->result_array();
    }
        
    function get_criptoativos_wid($params = array())
    {
        $this->db->select('criptoativos.sigla crip, exchanges.sigla exg');
        $this->db->join('exchanges', 'exchanges.idexchange = criptoativos.idexchange','LEFT');
        $this->db->order_by('idcriptoativo', 'desc');
        if(isset($params) && !empty($params))
        {
            if($params['idexchange'] != 0 && $params['idexchange'] != ''){
                $this->db->where_in('exchanges.idexchange', $params['idexchange']);
            }
            if($params['idcriptoativo'] != 0 && $params['idcriptoativo'] != ''){
                $this->db->where_in('idcriptoativo', $params['idcriptoativo']);
            }
        }
        return $this->db->get('criptoativos')->result_array();
    }

    /*
     * function to add new criptoativo
     */
    function add_criptoativo($params)
    {
        $this->db->insert('criptoativos',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update criptoativo
     */
    function update_criptoativo($idcriptoativo,$params)
    {
        $this->db->where('idcriptoativo',$idcriptoativo);
        return $this->db->update('criptoativos',$params);
    }
    
    /*
     * function to delete criptoativo
     */
    function delete_criptoativo($idcriptoativo)
    {
        return $this->db->delete('criptoativos',array('idcriptoativo'=>$idcriptoativo));
    }
}
