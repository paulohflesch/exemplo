<?php
class Exchange_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get exchange by idexchange
     */
    function get_exchange($idexchange)
    {
        return $this->db->get_where('exchanges',array('idexchange'=>$idexchange))->row_array();
    }
    
    /*
     * Get all exchanges count
     */
    function get_all_exchanges_count()
    {
        $this->db->from('exchanges');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all exchanges
     */
    function get_all_exchanges($params = array())
    {
        $this->db->order_by('idexchange', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('exchanges')->result_array();
    }
        
    /*
     * function to add new exchange
     */
    function add_exchange($params)
    {
        $this->db->insert('exchanges',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update exchange
     */
    function update_exchange($idexchange,$params)
    {
        $this->db->where('idexchange',$idexchange);
        return $this->db->update('exchanges',$params);
    }
    
    /*
     * function to delete exchange
     */
    function delete_exchange($idexchange)
    {
        return $this->db->delete('exchanges',array('idexchange'=>$idexchange));
    }
}
