<?php
 
class Permissao_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_telas_permissoes($idusuario,$idtela)
    {
        return $this->db->get_where('permissoes',array('idusuario'=>$idusuario,'idtela'=>$idtela))->result_array();
    }

    function get_permissoes_by_usuario($idusuario)
    {
        return $this->db->get_where('permissoes',array('idusuario'=>$idusuario))->result_array();
    }

    function update_permissoes($params)
    {
        $this->db->where('idusuario',$params['idusuario']);
        $this->db->where('idtela',$params['idtela']);
        return $this->db->update('permissoes',$params);
    }

    function add_permissao($params)
    {
        $this->db->insert('permissoes',$params);
        return $this->db->insert_id();
    }
    
}
