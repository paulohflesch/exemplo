<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Dashboard</h3>
            </div>
            <div class="box-body">
                <div class="row clearfix">
                <?php echo form_open_multipart(''); ?>
                    <div class="col-md-2">
                    <label for="idexchange" class="control-label">Exchange</label>
						<div class="form-group">
							<select name="idexchange[]" multiple="multiple" class="mult-select" style="min-width:200px;">
	    						<?php 
		    					foreach($exchanges as $exchange)
			    				{
				    				$selected = ($exchange['idexchange'] == $this->input->post('idexchange')) ? ' selected="selected"' : "";
									echo '<option value="'.$exchange['idexchange'].'" '.$selected.'>'.$exchange['sigla'].'</option>';
								} 
								?>
							</select>
					    </div>
					</div>
                    <div class="col-md-2">
                        <label for="idcriptoativo" class="control-label">Criptoativo</label>
					    <div class="form-group">
						    <select name="idcriptoativo[]" class="mult-select" multiple="multiple" style="min-width:200px;">
                                <?php 
		    					foreach($criptoativos as $criptoativo)
			    				{
				    				$selected = ($criptoativo['idcriptoativo'] == $this->input->post('idcriptoativo')) ? ' selected="selected"' : "";
									echo '<option value="'.$criptoativo['idcriptoativo'].'" '.$selected.'>'.$criptoativo['sigla'].'</option>';
								} 
								?>
						    </select>
						</div>
					</div>
					<div class="col-md-3">
                    <label for="intervalo" class="control-label">Intervalo</label>
						<div class="form-group">
							<select name="intervalo" class="mult-select" style="min-width:200px;">
								<option value="1m" <?php echo ($intervalo == '1m') ? 'selected="selected"' : '';?>>1 minuto</option>
								<option value="5m" <?php echo ($intervalo == '5m') ? 'selected="selected"' : '';?>>5 minutos</option>
								<option value="15m" <?php echo ($intervalo == '15m') ? 'selected="selected"' : '';?>>15 minutos</option>
								<option value="30m" <?php echo ($intervalo == '30m') ? 'selected="selected"' : '';?>>30 minutos</option>
								<option value="1h" <?php echo ($intervalo == '1h') ? 'selected="selected"' : '';?>>1 hora</option>
								<option value="2h" <?php echo ($intervalo == '2h') ? 'selected="selected"' : '';?>>2 horas</option>
								<option value="4h" <?php echo ($intervalo == '4h') ? 'selected="selected"' : '';?>>4 horas</option>
								<option value="1D" <?php echo ($intervalo == '1D') ? 'selected="selected"' : '';?>>1 dia</option>
								<option value="1W" <?php echo ($intervalo == '1W') ? 'selected="selected"' : '';?>>1 semana</option>
								<option value="1M" <?php echo ($intervalo == '1M') ? 'selected="selected"' : '';?>>1 mês</option>
							</select>
							<button type="submit" class="btn btn-success">
            		        	<i class="fa fa-search"></i> Filtrar
            	        	</button>
					    </div>
					</div>
                    <?php echo form_close(); ?>
                    <div class="col-md-12">
                        <?php 
		    			foreach($widgets as $widget)
			    		{
                        ?>
                            <div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
                                <label><a href="<?php echo site_url('dashboard/detalhe/').$widget['exg'].':'.$widget['crip'];?>"><?php echo $widget['exg'].' - '.$widget['crip']; ?></a></label>
								<!-- TradingView Widget BEGIN -->
								<div class="tradingview-widget-container">
									<div class="tradingview-widget-container__widget"></div>
									<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
										{
										"interval": "<?php echo $intervalo; ?>",
										"width": "100%",
										"isTransparent": false,
										"height":"450",
										"symbol": "<?php echo $widget['exg'].':'.$widget['crip']; ?>",
										"showIntervalTabs": true,
										"locale": "br",
										"colorTheme": "light"
										}
									</script>
								</div>
                                <hr/>
							</div>
							<!-- TradingView Widget END -->
						<?php 
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>