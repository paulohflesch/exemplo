<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Criptoativos</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('criptoativo/add'); ?>" class="btn btn-success btn-sm">Adicionar</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id</th>
						<th>Exchange</th>
						<th>Sigla</th>
						<th>Ativo</th>
						<th>Ações</th>
                    </tr>
                    <?php foreach($criptoativos as $c){ ?>
                    <tr>
						<td><?php echo $c['idcriptoativo']; ?></td>
						<td><?php echo $c['exg']; ?></td>
						<td><?php echo $c['sigla']; ?></td>
						<td><?php echo ($c['ativo'] == 1) ? 'Sim' : 'Não'; ?></td>
						<td>
                            <a href="<?php echo site_url('criptoativo/edit/'.$c['idcriptoativo']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Editar</a> 
                            <?php
                                echo ($c['ativo'] == 1) ? '<a href="'.site_url('criptoativo/remove/'.$c['idcriptoativo']).'" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Deletar</a>' : '';
                            ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
