<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Criptoativo</h3>
            </div>
			<?php echo form_open('criptoativo/edit/'.$criptoativo['idcriptoativo']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="sigla" class="control-label">Sigla</label>
						<div class="form-group">
							<input type="text" name="sigla" value="<?php echo ($this->input->post('sigla') ? $this->input->post('sigla') : $criptoativo['sigla']); ?>" class="form-control" id="sigla" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="idexchange" class="control-label">Exchange</label>
						<div class="form-group">
							<select name="idexchange" class="form-control">
								<option value="">Selecione uma Exchange</option>
								<?php 
								foreach($all_exchanges as $exchange)
								{
									$selected = ($exchange['idexchange'] == $criptoativo['idexchange']) ? ' selected="selected"' : "";

									echo '<option value="'.$exchange['idexchange'].'" '.$selected.'>'.$exchange['sigla'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="ativo" class="control-label">Ativo</label>
						<div class="form-group">
						<input type="checkbox" name="ativo" value="1" <?php echo ($exchange['ativo']==1 ? 'checked="checked"' : ''); ?> id='ativo' />
						</div>
					</div>
				</div>
			</div>
			<?php foreach($permissoes as $permissao){ 
                if($permissao['idtela'] == 1 && $permissao['editar'] == 1){
            ?>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Salvar
				</button>
	        </div>
			<?php }
                } 
            ?>		
			<?php echo form_close(); ?>
		</div>
    </div>
</div>