<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Usuário</h3>
            </div>
			<?php $atributos = ['autocomplete' => 'off'];
			echo form_open('usuario/edit/'.$usuario['idusuario'], $atributos); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="nome" class="control-label">Nome</label>
						<div class="form-group">
							<input type="text" name="nome" value="<?php echo ($this->input->post('nome') ? $this->input->post('nome') : $usuario['nome']); ?>" class="form-control" id="nome" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $usuario['email']); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="senha" class="control-label">Senha</label>
						<div class="form-group">
							<input type="password" name="senha" value="" class="form-control" id="senha" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="ativo" class="control-label">Ativo</label>
						<div class="form-group">
							<input type="checkbox" name="ativo" value="1" <?php echo ($usuario['ativo']==1 ? 'checked="checked"' : ''); ?> id='ativo' />
						</div>
					</div>
					<div class="col-md-12">
						<div class="box-permissoes">
							<label for="nome" class="control-label">Telas/Permissões</label>
							<table class="table table-striped">
								<tr>
									<th>Tela</th>
									<th>Listar</th>
									<th>Adicionar</th>
									<th>Editar</th>
									<th>Excluir</th>
								<?php
								foreach($telas as $tela)
								{
									echo '<tr>';
									echo '<td>'.$tela['descricao'].'</td>';
									$checkedListar = '';
									foreach($permissoes_usu as $permissao){
										if($tela['idtela'] == $permissao['idtela']){
											if($permissao['ver'] == 1){
												$checkedListar = 'checked';
											}
										}
									}
									echo '<td><input type="checkbox" name="listar[]" value="'.$tela['idtela'].'" '.$checkedListar.' /></td>';
									$checkedAdicionar = '';
									foreach($permissoes_usu as $permissao){
										if($tela['idtela'] == $permissao['idtela']){
											if($permissao['adicionar'] == 1){
												$checkedAdicionar = 'checked';
											}
										}
									}
									echo '<td><input type="checkbox" name="adicionar[]" value="'.$tela['idtela'].'" '.$checkedAdicionar.' /></td>';
									$checkedEditar = '';
									foreach($permissoes_usu as $permissao){
										if($tela['idtela'] == $permissao['idtela']){
											if($permissao['editar'] == 1){
												$checkedEditar = 'checked';
											}
										}
									}
									echo '<td><input type="checkbox" name="editar[]" value="'.$tela['idtela'].'" '.$checkedEditar.' /></td>';
									$checkedExcluir = '';
									foreach($permissoes_usu as $permissao){
										if($tela['idtela'] == $permissao['idtela']){
											if($permissao['excluir'] == 1){
												$checkedExcluir = 'checked';
											}
										}
									}
									echo '<td><input type="checkbox" name="excluir[]" value="'.$tela['idtela'].'" '.$checkedExcluir.' /></td>';
								} 
								?>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php 
			foreach($permissoes as $permissao){ 
                if($permissao['idtela'] == 5 && $permissao['editar'] == 1){
            ?>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Salvar
				</button>
	        </div>
			<?php }
                } 
            ?>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>