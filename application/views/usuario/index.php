<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Usuários</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('usuario/add'); ?>" class="btn btn-success btn-sm">Adicionar</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id</th>
						<th>Nome</th>
						<th>Email</th>
						<th>Criado</th>
						<th>Ativo</th>
						<th>Ações</th>
                    </tr>
                    <?php foreach($usuarios as $u){ ?>
                    <tr>
						<td><?php echo $u['idusuario']; ?></td>
						<td><?php echo $u['nome']; ?></td>
						<td><?php echo $u['email']; ?></td>
						<td><?php echo date('d/m/Y',strtotime($u['criado'])); ?></td>
						<td><?php echo ($u['ativo'] == 1) ? 'Sim' : 'Não'; ?></td>
						<td>
                            <a href="<?php echo site_url('usuario/edit/'.$u['idusuario']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Editar</a> 
                            <a href="<?php echo site_url('usuario/remove/'.$u['idusuario']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Deletar</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
