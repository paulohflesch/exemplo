<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Usuário</h3>
            </div>
            <?php $atributos = ['autocomplete' => 'off'];
			echo form_open('usuario/add', $atributos); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="nome" class="control-label">Nome</label>
						<div class="form-group">
							<input type="text" name="nome" value="<?php echo $this->input->post('nome'); ?>" class="form-control" id="nome" required />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="senha" class="control-label">Senha</label>
						<div class="form-group">
							<input type="password" name="senha" value="<?php echo $this->input->post('senha'); ?>" class="form-control" id="senha" />
						</div>
					</div>
					<div class="col-md-12">
						<div class="box-permissoes">
							<label for="nome" class="control-label">Telas/Permissões</label>
							<table class="table table-striped">
								<tr>
									<th>Tela</th>
									<th>Listar</th>
									<th>Adicionar</th>
									<th>Editar</th>
									<th>Excluir</th>
								<?php
								foreach($telas as $tela)
								{
									echo '<tr>';
									echo '<td>'.$tela['descricao'].'</td>';	
									echo '<td><input type="checkbox" name="listar[]" value="'.$tela['idtela'].'" checked /></td>';
									echo '<td><input type="checkbox" name="adicionar[]" value="'.$tela['idtela'].'" checked /></td>';
									echo '<td><input type="checkbox" name="editar[]" value="'.$tela['idtela'].'" checked /></td>';
									echo '<td><input type="checkbox" name="excluir[]" value="'.$tela['idtela'].'" checked /></td>';
								} 
								?>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php foreach($permissoes as $permissao){ 
                if($permissao['idtela'] == 5 && $permissao['adicionar'] == 1){
            ?>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Salvar
            	</button>
          	</div>
			  <?php }
                } 
            ?>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>