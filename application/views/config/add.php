<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Exchange</h3>
            </div>
            <?php echo form_open('exchange/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="sigla" class="control-label">Sigla</label>
						<div class="form-group">
							<input type="text" name="sigla" value="<?php echo $this->input->post('sigla'); ?>" class="form-control" id="sigla" />
						</div>
					</div>
				</div>
			</div>
			<?php foreach($permissoes as $permissao){ 
                if($permissao['idtela'] == 3 && $permissao['adicionar'] == 1){
            ?>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Salvar
            	</button>
          	</div>
			  <?php }
                } 
            ?>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>