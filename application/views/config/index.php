<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Configuração</h3>
            </div>
			<?php echo form_open('config/edit/'); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-12">
						<div class="col-md-3">
							<label for="intervalo" class="control-label">Intervalo</label>
							<div class="form-group">
								<select name="intervalo" class="form-control">
									<option value="1m" <?php echo ($intervalo['valor'] == '1m') ? 'selected="selected"' : '';?>>1 minuto</option>
									<option value="5m" <?php echo ($intervalo['valor'] == '5m') ? 'selected="selected"' : '';?>>5 minutos</option>
									<option value="15m" <?php echo ($intervalo['valor'] == '15m') ? 'selected="selected"' : '';?>>15 minutos</option>
									<option value="30m" <?php echo ($intervalo['valor'] == '30m') ? 'selected="selected"' : '';?>>30 minutos</option>
									<option value="1h" <?php echo ($intervalo['valor'] == '1h') ? 'selected="selected"' : '';?>>1 hora</option>
									<option value="2h" <?php echo ($intervalo['valor'] == '2h') ? 'selected="selected"' : '';?>>2 horas</option>
									<option value="4h" <?php echo ($intervalo['valor'] == '4h') ? 'selected="selected"' : '';?>>4 horas</option>
									<option value="1D" <?php echo ($intervalo['valor'] == '1D') ? 'selected="selected"' : '';?>>1 dia</option>
									<option value="1W" <?php echo ($intervalo['valor'] == '1W') ? 'selected="selected"' : '';?>>1 semana</option>
									<option value="1M" <?php echo ($intervalo['valor'] == '1M') ? 'selected="selected"' : '';?>>1 mês</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3">
							<label for="col_dash" class="control-label">Colunas no Dashboard</label>
							<div class="form-group">
								<select name="col_dash" class="form-control">
									<option value="1" <?php echo ($col_dash['valor'] == '1') ? 'selected="selected"' : '';?>>1</option>
									<option value="2" <?php echo ($col_dash['valor'] == '2') ? 'selected="selected"' : '';?>>2</option>
									<option value="3" <?php echo ($col_dash['valor'] == '3') ? 'selected="selected"' : '';?>>3</option>
									<option value="4" <?php echo ($col_dash['valor'] == '4') ? 'selected="selected"' : '';?>>4</option>
									<option value="5" <?php echo ($col_dash['valor'] == '5') ? 'selected="selected"' : '';?>>5</option>
									<option value="6" <?php echo ($col_dash['valor'] == '6') ? 'selected="selected"' : '';?>>6</option>
									<option value="7" <?php echo ($col_dash['valor'] == '7') ? 'selected="selected"' : '';?>>7</option>
									<option value="8" <?php echo ($col_dash['valor'] == '8') ? 'selected="selected"' : '';?>>8</option>
									<option value="9" <?php echo ($col_dash['valor'] == '9') ? 'selected="selected"' : '';?>>9</option>
									<option value="10" <?php echo ($col_dash['valor'] == '10') ? 'selected="selected"' : '';?>>10</option>
									<option value="11" <?php echo ($col_dash['valor'] == '11') ? 'selected="selected"' : '';?>>11</option>
									<option value="12" <?php echo ($col_dash['valor'] == '12') ? 'selected="selected"' : '';?>>12</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php foreach($permissoes as $permissao){ 
                if($permissao['idtela'] == 3 && $permissao['editar'] == 1){
            ?>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Salvar
				</button>
	        </div>	
			<?php }
                } 
            ?>			
			<?php echo form_close(); ?>
		</div>
    </div>
</div>