<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Dashboard</h3>
            </div>
            <div class="box-body">
                <div class="row clearfix">
                <?php echo form_open_multipart(''); ?>
                    <?php echo form_close(); ?>
                    <div class="col-md-12">
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>1 minuto</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "1m",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>5 minuto</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "5m",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>15 minuto</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "15m",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>30 minuto</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "30m",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>1 hora</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "1h",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>2 horas</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "2h",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>4 horas</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "4h",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>1 dia</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "1D",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>1 semana</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "1W",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
						<div class="col-md-4" style="width:<?php echo $col_dash.'%'; ?>">
							<label>1 mês</label>
							<!-- TradingView Widget BEGIN -->
							<div class="tradingview-widget-container">
								<div class="tradingview-widget-container__widget"></div>
								<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
									{
									"interval": "1M",
									"width": "100%",
									"isTransparent": false,
									"height":"450",
									"symbol": "<?php echo $widget; ?>",
									"showIntervalTabs": true,
									"locale": "br",
									"colorTheme": "light"
									}
								</script>
							</div>
							<hr/>
						</div>
						<!-- TradingView Widget END -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>