<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Exchanges</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('exchange/add'); ?>" class="btn btn-success btn-sm">Adicionar</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id</th>
						<th>Sigla</th>
						<th>Ativo</th>
						<th>Ações</th>
                    </tr>
                    <?php foreach($exchanges as $e){ ?>
                    <tr>
						<td><?php echo $e['idexchange']; ?></td>
						<td><?php echo $e['sigla']; ?></td>
						<td><?php echo ($e['ativo'] == 1) ? 'Sim' : 'Não'; ?></td>
						<td>
                            <a href="<?php echo site_url('exchange/edit/'.$e['idexchange']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Editar</a> 
                            <a href="<?php echo site_url('exchange/remove/'.$e['idexchange']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Deletar</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
