<?php

class Auth{

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('session');
        $this->CI->load->helper('url');
    }

    function logged() {
        $logged = $this->CI->session->userdata('isLoggedIn');
        if (!isset($logged) || $logged != true) {
            if(current_url() != base_url()){
                redirect('/');
            }
        }
    }
}