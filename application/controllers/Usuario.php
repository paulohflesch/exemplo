<?php 
class Usuario extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Usuario_model');
        $this->load->model('Tela_model');
        $this->load->model('Permissao_model');
        $this->load->model('Log_model');
        $this->load->library('session');
    } 

    /*
     * Listing of usuarios
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('usuario/index?');
        $config['total_rows'] = $this->Usuario_model->get_all_usuarios_count();
        $this->pagination->initialize($config);

        $data['usuarios'] = $this->Usuario_model->get_all_usuarios($params);
        $data['permissoes'] = $this->session->userdata('permissoes');
        $data['_view'] = 'usuario/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new usuario
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nome' => $this->input->post('nome'),
				'email' => $this->input->post('email'),
				'senha' => password_hash($this->input->post('senha'), PASSWORD_DEFAULT),             
            );
           $usuario_id = $this->Usuario_model->add_usuario($params);

            $paramsLog = array(
                'tabela' => 'usuario',
                'acao' => 'insert',
                'idusuario' =>  $this->session->userdata('idusuario'),
                'registro' => $usuario_id,
            );
            $this->Log_model->add_log($paramsLog);

           $data['telas'] = $this->Tela_model->get_all_telas();
           $adicionar = $this->input->post('adicionar');
           $listar = $this->input->post('listar');
           $editar = $this->input->post('editar');
           $excluir = $this->input->post('excluir');
           foreach ($data['telas'] as $tela) {
                $addInt = 0;
                $listarInt = 0;
                $editarInt = 0;
                $excluirInt = 0;
                foreach($adicionar as $add){
                    if($tela['idtela'] == $add){
                        $addInt = 1;
                        break;
                    }
                }
                foreach($listar as $lst){
                    if($tela['idtela'] == $lst){
                        $listarInt = 1;
                        break;
                    }
                }
                foreach($editar as $edt){
                    if($tela['idtela'] == $edt){
                        $editarInt = 1;
                        break;
                    }
                }
                foreach($excluir as $exc){
                    if($tela['idtela'] == $exc){
                        $excluirInt = 1;
                        break;
                    }
                }
                $paramsPermissoes = array(
                    'adicionar' => $addInt,
                    'ver' => $listarInt,
                    'editar' => $editarInt,
                    'excluir' => $excluirInt,
                    'idusuario' => $usuario_id,
                    'idtela' => (int)$tela['idtela'],
                );
               $this->Permissao_model->add_permissao($paramsPermissoes);
            }
            redirect('usuario/index');
        }
        else
        {   
            $data['telas'] = $this->Tela_model->get_all_telas();
            $data['permissoes'] = $this->session->userdata('permissoes');
            $data['_view'] = 'usuario/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a usuario
     */
    function edit($idusuario)
    {   
        // check if the usuario exists before trying to edit it
        $data['usuario'] = $this->Usuario_model->get_usuario($idusuario);
        
        if(isset($data['usuario']['idusuario']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nome' => $this->input->post('nome'),
					'email' => $this->input->post('email'),
					'ativo' => ($this->input->post('ativo') == 1) ? 1 : 0,
                );
                // Aqui valida se precisa ou não alterar a senha
                if(!password_verify($this->input->post('senha'), $data['usuario']['senha'])){
                    $params['senha'] = password_hash($this->input->post('senha'), PASSWORD_DEFAULT);
                }

                $this->Usuario_model->update_usuario($idusuario,$params);

                $paramsLog = array(
                    'tabela' => 'usuario',
                    'acao' => 'update',
                    'idusuario' =>  $this->session->userdata('idusuario'),
                    'registro' => $idusuario,
                );
                $this->Log_model->add_log($paramsLog);

                $data['telas'] = $this->Tela_model->get_all_telas();
                $adicionar = $this->input->post('adicionar');
                $listar = $this->input->post('listar');
                $editar = $this->input->post('editar');
                $excluir = $this->input->post('excluir');
                foreach ($data['telas'] as $tela) {
                        $addInt = 0;
                        $listarInt = 0;
                        $editarInt = 0;
                        $excluirInt = 0;
                        foreach($adicionar as $add){
                            if($tela['idtela'] == $add){
                                $addInt = 1;
                                break;
                            }
                        }
                        foreach($listar as $lst){
                            if($tela['idtela'] == $lst){
                                $listarInt = 1;
                                break;
                            }
                        }
                        foreach($editar as $edt){
                            if($tela['idtela'] == $edt){
                                $editarInt = 1;
                                break;
                            }
                        }
                        foreach($excluir as $exc){
                            if($tela['idtela'] == $exc){
                                $excluirInt = 1;
                                break;
                            }
                        }
                        $paramsPermissoes = array(
                            'adicionar' => $addInt,
                            'ver' => $listarInt,
                            'editar' => $editarInt,
                            'excluir' => $excluirInt,
                            'idusuario' => $data['usuario']['idusuario'],
                            'idtela' => (int)$tela['idtela'],
                        );
                    $this->Permissao_model->update_permissoes($paramsPermissoes);
                }          
                redirect('usuario/index');
            }
            else
            {
                $data['telas'] = $this->Tela_model->get_all_telas();
                $data['permissoes_usu'] = $this->Permissao_model->get_permissoes_by_usuario($idusuario);
                $data['permissoes'] = $this->session->userdata('permissoes');
                $data['_view'] = 'usuario/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The usuario you are trying to edit does not exist.');
    } 

    /*
     * Deleting usuario
     */
    function remove($idusuario)
    {
        $usuario = $this->Usuario_model->get_usuario($idusuario);

        // check if the usuario exists before trying to delete it
        if(isset($usuario['idusuario']))
        {
            $this->Usuario_model->delete_usuario($idusuario);
            redirect('usuario/index');
        }
        else
            show_error('The usuario you are trying to delete does not exist.');
    }
    
}
