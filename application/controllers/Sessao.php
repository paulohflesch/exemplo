<?php
if (!isset($_SESSION)) session_start();
if (!isset($_SESSION['usuario'])) {
    $config = new CI_Config();
    header('location: ../');
    exit();
}