<?php

class Logout extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }
    function index(){
        if (!isset($_SESSION)) session_start();
        if (isset($_SESSION['usuario'])) {
            session_destroy();
        }
        redirect('../');
    }
}