<?php
class Login extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Usuario_model');
        $this->load->model('Permissao_model');
        $this->load->model('Log_model');
        $this->load->library('session');
    }

    function index(){
        $this->load->view('login/index');
    }
   
    function auth(){
        $senha = $this->input->post('senha');
        $email = $this->input->post('email');
        $data['usuario'] = $this->Usuario_model->get_usuario_email($email);
        $verify_pass = password_verify($senha, $data['usuario']['senha']);
        if($verify_pass){
            $data['permissoes'] = $this->Permissao_model->get_permissoes_by_usuario($data['usuario']['idusuario']);
            $permissoes = $this->setPermissionSession($data['permissoes']);
            $session = $this->setUserSession($data['usuario']);

            $paramsLog = array(
                'tabela' => 'login',
                'acao' => 'login',
                'idusuario' =>  $this->session->userdata('idusuario'),
                'registro' => '',
            );
            $this->Log_model->add_log($paramsLog);

            redirect('dashboard/index');
        }else{
            redirect('/?msg=Usuário e senha inválidos');
        }
    }

    private function setUserSession($user)
    {
        $data = [
            'idusuario' => $user['idusuario'],
            'nome' => $user['nome'],
            'email' => $user['email'],
            'isLoggedIn' => true,
        ];

        $this->session->set_userdata($data);
        return true;
    }

    private function setPermissionSession($permission)
    {
        $data = [
            'permissoes' => $permission,
        ];

        $this->session->set_userdata($data);
        return true;
    }

    function logout()
    {
        $this->session->unset_userdata('isLoggedIn');
        redirect('/');
    }
} 