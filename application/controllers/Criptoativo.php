<?php
 
class Criptoativo extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Criptoativo_model');
        $this->load->model('Log_model');
        $this->load->library('session');
    } 

    /*
     * Listing of criptoativos
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('criptoativo/index?');
        $config['total_rows'] = $this->Criptoativo_model->get_all_criptoativos_count();
        $this->pagination->initialize($config);

        $data['criptoativos'] = $this->Criptoativo_model->get_all_criptoativos($params);
        
        $data['permissoes'] = $this->session->userdata('permissoes');
        $data['_view'] = 'criptoativo/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new criptoativo
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'idexchange' => $this->input->post('idexchange'),
				'sigla' => $this->input->post('sigla'),
            );
            
            $criptoativo_id = $this->Criptoativo_model->add_criptoativo($params);

            $paramsLog = array(
                'tabela' => 'criptoativo',
                'acao' => 'insert',
                'idusuario' =>  $this->session->userdata('idusuario'),
                'registro' => $criptoativo_id,
            );
            $this->Log_model->add_log($paramsLog);

            redirect('criptoativo/index');
        }
        else
        {
			$this->load->model('Exchange_model');
			$data['all_exchanges'] = $this->Exchange_model->get_all_exchanges();
            $data['permissoes'] = $this->session->userdata('permissoes');
            $data['_view'] = 'criptoativo/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a criptoativo
     */
    function edit($idcriptoativo)
    {   
        // check if the criptoativo exists before trying to edit it
        $data['criptoativo'] = $this->Criptoativo_model->get_criptoativo($idcriptoativo);
        
        if(isset($data['criptoativo']['idcriptoativo']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'idexchange' => $this->input->post('idexchange'),
					'sigla' => $this->input->post('sigla'),
					'ativo' => ($this->input->post('ativo') == 1) ? 1 : 0,
                );

                $this->Criptoativo_model->update_criptoativo($idcriptoativo,$params);

                $paramsLog = array(
                    'tabela' => 'criptoativo',
                    'acao' => 'update',
                    'idusuario' =>  $this->session->userdata('idusuario'),
                    'registro' => $idcriptoativo,
                );
                $this->Log_model->add_log($paramsLog);

                redirect('criptoativo/index');
            }
            else
            {
				$this->load->model('Exchange_model');
				$data['all_exchanges'] = $this->Exchange_model->get_all_exchanges();
                $data['permissoes'] = $this->session->userdata('permissoes');
                $data['_view'] = 'criptoativo/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The criptoativo you are trying to edit does not exist.');
    } 

    /*
     * Deleting criptoativo
     */
    function remove($idcriptoativo)
    {
        $criptoativo = $this->Criptoativo_model->get_criptoativo($idcriptoativo);

        // check if the criptoativo exists before trying to delete it
        if(isset($criptoativo['idcriptoativo']))
        {
            $params = array(
                'ativo' => 0,
            );
            $this->Criptoativo_model->update_criptoativo($idcriptoativo,$params);
            redirect('criptoativo/index');
        }
        else
            show_error('The criptoativo you are trying to delete does not exist.');
    }
    
}
