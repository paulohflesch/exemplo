<?php
 
class Exchange extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Exchange_model');
        $this->load->library('session');
    } 

    /*
     * Listing of exchanges
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('exchange/index?');
        $config['total_rows'] = $this->Exchange_model->get_all_exchanges_count();
        $this->pagination->initialize($config);

        $data['exchanges'] = $this->Exchange_model->get_all_exchanges($params);
        $data['permissoes'] = $this->session->userdata('permissoes');
        $data['_view'] = 'exchange/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new exchange
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'sigla' => $this->input->post('sigla'),
            );
            
            $exchange_id = $this->Exchange_model->add_exchange($params);

            $paramsLog = array(
                'tabela' => 'exchanges',
                'acao' => 'insert',
                'idusuario' =>  $this->session->userdata('idusuario'),
                'registro' => $exchange_id,
            );
            $this->Log_model->add_log($paramsLog);

            redirect('exchange/index');
        }
        else
        {    
            $data['permissoes'] = $this->session->userdata('permissoes');
            $data['_view'] = 'exchange/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a exchange
     */
    function edit($idexchange)
    {   
        // check if the exchange exists before trying to edit it
        $data['exchange'] = $this->Exchange_model->get_exchange($idexchange);
        
        if(isset($data['exchange']['idexchange']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'sigla' => $this->input->post('sigla'),
					'ativo' => ($this->input->post('ativo') == 1) ? 1 : 0,
                );

                $this->Exchange_model->update_exchange($idexchange,$params);

                $paramsLog = array(
                    'tabela' => 'exchanges',
                    'acao' => 'update',
                    'idusuario' =>  $this->session->userdata('idusuario'),
                    'registro' => $exchange_id,
                );
                $this->Log_model->add_log($paramsLog);

                redirect('exchange/index');
            }
            else
            {
                $data['permissoes'] = $this->session->userdata('permissoes');
                $data['_view'] = 'exchange/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The exchange you are trying to edit does not exist.');
    } 

    /*
     * Deleting exchange
     */
    function remove($idexchange)
    {
        $exchange = $this->Exchange_model->get_exchange($idexchange);

        // check if the exchange exists before trying to delete it
        if(isset($exchange['idexchange']))
        {
            $this->Exchange_model->delete_exchange($idexchange);
            redirect('exchange/index');
        }
        else
            show_error('The exchange you are trying to delete does not exist.');
    }
    
}
