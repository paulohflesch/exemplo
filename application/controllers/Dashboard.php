<?php
 
class Dashboard extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Criptoativo_model');
        $this->load->model('Exchange_model');
        $this->load->model('Config_model');
        $this->load->library('session');
    }

    function index()
    {
        $data['exchanges'] = $this->Exchange_model->get_all_exchanges();
        $data['criptoativos'] = $this->Criptoativo_model->get_all_criptoativos();

        $params['idcriptoativo'] = $this->input->post('idcriptoativo');
        $params['idexchange'] = $this->input->post('idexchange');
        $data['intervalo'] = $this->input->post('intervalo');
        $data['intervalo_bd'] = $this->Config_model->get_config('intervalo');
        if(empty($data['intervalo'])){
            $data['intervalo'] = $data['intervalo_bd']['valor'];
        }

        $data['col_dash'] = $this->Config_model->get_config('col_dash');
        $data['col_dash'] = 100 / (int)$data['col_dash']['valor'];        

        $data['widgets'] = $this->Criptoativo_model->get_criptoativos_wid($params);
        $data['_view'] = 'dashboard';
        $data['permissoes'] = $this->session->userdata('permissoes');
        $this->load->view('layouts/main',$data);
    }

    function detalhe($widget){
        $data['col_dash'] = $this->Config_model->get_config('col_dash');
        $data['col_dash'] = 100 / (int)$data['col_dash']['valor'];    
        $data['_view'] = 'dashboard_detalhe';
        $data['widget'] = $widget;
        $data['permissoes'] = $this->session->userdata('permissoes');
        $this->load->view('layouts/main',$data);
    }
}
