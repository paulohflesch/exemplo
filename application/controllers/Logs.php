<?php
 
class Logs extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('directory');
        $this->load->library('session');
        $this->load->model('Log_model');
    } 

    function index()
    {
        $data['permissoes'] = $this->session->userdata('permissoes');

        $data['logs'] = $this->Log_model->get_logs();

        $data['_view'] = 'logs/index';
        $this->load->view('layouts/main',$data);
    }

    function view($fileName){

        $data['permissoes'] = $this->session->userdata('permissoes');
        $data['file'] = file_get_contents(APPPATH.'/logs/'.$fileName);

        $data['_view'] = 'logs/view';
        $this->load->view('layouts/main',$data);
    }
}