<?php

class Empresa extends CI_Controller{
    function __construct()
    {
        include_once 'Sessao.php';
        parent::__construct();
        $this->load->model('Empresa_model');
    } 

    /*
     * Listing of empresa
     */
    function index()
    {
        $data['empresa'] = $this->Empresa_model->get_all_empresa();
        
        $data['_view'] = 'empresa/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new empresa
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nome' => $this->input->post('nome'),
            );
            
            $empresa_id = $this->Empresa_model->add_empresa($params);
            redirect('empresa/index');
        }
        else
        {            
            $data['_view'] = 'empresa/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a empresa
     */
    function edit($id)
    {   
        // check if the empresa exists before trying to edit it
        $data['empresa'] = $this->Empresa_model->get_empresa($id);
        
        if(isset($data['empresa']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nome' => $this->input->post('nome'),
                );

                $this->Empresa_model->update_empresa($id,$params);            
                redirect('empresa/index');
            }
            else
            {
                $data['_view'] = 'empresa/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The empresa you are trying to edit does not exist.');
    } 

    /*
     * Deleting empresa
     */
    function remove($id)
    {
        $empresa = $this->Empresa_model->get_empresa($id);

        // check if the empresa exists before trying to delete it
        if(isset($empresa['id']))
        {
            $this->Empresa_model->delete_empresa($id);
            redirect('empresa/index');
        }
        else
            show_error('The empresa you are trying to delete does not exist.');
    }
    
}
