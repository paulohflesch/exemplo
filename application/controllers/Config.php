<?php
 
class Config extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Config_model');
        $this->load->model('Permissao_model');
        $this->load->model('Log_model');
        $this->load->library('session');
    } 

    function index()
    {
        $data['intervalo'] = $this->Config_model->get_config('intervalo');
        $data['col_dash'] = $this->Config_model->get_config('col_dash');
        $data['permissoes'] = $this->session->userdata('permissoes');
        $data['_view'] = 'config/index';
        $this->load->view('layouts/main',$data);
    }

    function edit()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'valor' => $this->input->post('intervalo'),
            );
            $this->Config_model->update_config('intervalo',$params);

            $paramsLog = array(
                'tabela' => 'config',
                'acao' => 'update',
                'idusuario' =>  $this->session->userdata('idusuario'),
                'registro' => 'intervalo para - '.$params['valor'],
            );
            $this->Log_model->add_log($paramsLog);
            
            $params = array(
                'valor' => $this->input->post('col_dash'),
            );

            $this->Config_model->update_config('col_dash',$params);  

            $paramsLog = array(
                'tabela' => 'config',
                'acao' => 'update',
                'idusuario' =>  $this->session->userdata('idusuario'),
                'registro' => 'col_dash para - '.$params['valor'],
            );
            $this->Log_model->add_log($paramsLog);
            
            redirect('config/index');
        }
        else
        {
            $data['permissoes'] = $this->session->userdata('permissoes');
            $data['_view'] = 'config/index';
            $this->load->view('layouts/main',$data);
        }
     
    } 
    
}
